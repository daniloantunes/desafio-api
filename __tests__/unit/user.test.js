const { factory } = require("../factories");

describe("User", () => {
  it("should encrypt user password", async () => {
    const user = await factory.create("User", { password: "danilo" });
    const hashVerify = user.checkPassword("danilo");
    expect(hashVerify).toBe(true);
  });
});
