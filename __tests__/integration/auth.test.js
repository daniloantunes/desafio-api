const request = require("supertest");
const app = require("../../src/app");
const { factory } = require("../factories");
const truncate = require("../utils/truncate");

describe("Authentication", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should authenticate with valid credentials", async () => {
    const password = "6798795465";
    const user = await factory.create("User", { password });
    const response = await request(app)
      .post("/auth/login")
      .send({ email: user.email, password });
    expect(response.status).toBe(200);
  });

  it("should return jwt token when authenticated", async () => {
    const password = "6798795465";
    const user = await factory.create("User", { password });
    const response = await request(app)
      .post("/auth/login")
      .send({ email: user.email, password });
    expect(response.body).toHaveProperty("token");
    expect(response.body).toHaveProperty("user");
  });

  it("should authenticate with invalid user", async () => {
    const response = await request(app)
      .post("/auth/login")
      .send({ email: "email", password: "aaaaaaa" });
    expect(response.status).toBe(401);
  });

  it("should authenticate with invalid password", async () => {
    const user = await factory.create("User", { password: "danilo" });
    const response = await request(app)
      .post("/auth/login")
      .send({ email: user.email, password: "aaaaaaa" });
    expect(response.status).toBe(401);
  });

  it("should be able to access private routes when authenticated", async () => {
    const user = await factory.create("User", { password: "danilo" });
    const response = await request(app)
      .get("/tools")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
  });

  it("should not be able to access private routes without jwt token", async () => {
    const response = await request(app).get("/tools");
    expect(response.status).toBe(401);
  });

  it("should not be able to access private routes with invalid jwt token", async () => {
    const response = await request(app)
      .get("/tools")
      .set("Authorization", "dfasdfasdsdf");
    expect(response.status).toBe(401);
  });

  it("should authenticate with valid jwt token", async () => {
    const user = await factory.create("User", { password: "danilo" });
    const response = await request(app)
      .put("/auth/login")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
  });

  it("should return jwt token when authenticated with jwt token", async () => {
    const user = await factory.create("User", { password: "danilo" });
    const response = await request(app)
      .put("/auth/login")
      .set("Authorization", user.generateToken());
    expect(response.body).toHaveProperty("token");
  });

  it("should not authenticate without jwt token", async () => {
    const response = await request(app).put("/auth/login");
    expect(response.status).toBe(401);
  });

  it("should not authenticate with invalid jwt token", async () => {
    const response = await request(app)
      .put("/auth/login")
      .set("Authorization", "ieupqrouasldfasfasd");
    expect(response.status).toBe(401);
  });
});
