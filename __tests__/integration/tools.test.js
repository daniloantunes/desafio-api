const request = require("supertest");
const app = require("../../src/app");
const { factory, faker } = require("../factories");
const truncate = require("../utils/truncate");

describe("Tools", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should be able to create a tool", async () => {
    const user = await factory.create("User");
    const title = faker.lorem.word();
    const link = faker.image.imageUrl();
    const description = faker.lorem.text();
    const tags = ["Tag1", "Tag2"];
    const response = await request(app)
      .post("/tools")
      .send({ title, link, description, tags })
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("id");
    const tool = await app.models.Tool.findByPk(response.body.id);
    const tagsDb = await app.models.ToolTag.findAll({
      where: { tool_id: tool.id },
    });
    expect(!!tool).toBe(true);
    expect(tool.title).toBe(title);
    expect(tool.link).toBe(link);
    expect(tool.description).toBe(description);
    expect(tagsDb.length).toBe(tags.length);
  });

  it("should be able to update a tool", async () => {
    const user = await factory.create("User");
    const tool = await factory.create("Tool", { user_id: user.id });
    await factory.create("ToolTag", { tool_id: tool.id });
    await factory.create("ToolTag", { tool_id: tool.id });
    const title = faker.lorem.word();
    const link = faker.image.imageUrl();
    const description = faker.lorem.text();
    const tagsUpdate = ["Tag1", "Tag2", "Tag3", "Tag4", "Tag5"];
    const response = await request(app)
      .put(`/tools/${tool.id}`)
      .send({ title, link, description, tags: tagsUpdate })
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("id");
    const toolDb = await app.models.Tool.findByPk(tool.id);
    const tagsDb = await app.models.ToolTag.findAll({
      where: { tool_id: toolDb.id },
    });
    expect(!!toolDb).toBe(true);
    expect(toolDb.title).toBe(title);
    expect(toolDb.link).toBe(link);
    expect(toolDb.description).toBe(description);
    expect(tagsDb.length).toBe(tagsUpdate.length);
  });

  it("should not be able to update a inexistent tool", async () => {
    const user = await factory.create("User");
    const title = faker.lorem.word();
    const link = faker.image.imageUrl();
    const description = faker.lorem.text();
    const tags = ["Tag1", "Tag2", "Tag3", "Tag4", "Tag5"];
    const response = await request(app)
      .put(`/tools/54878`)
      .send({ title, link, description, tags })
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(404);
  });

  it("should be able to remove a existent tool", async () => {
    const user = await factory.create("User");
    const tool = await factory.create("Tool", { user_id: user.id });
    await factory.create("ToolTag", { tool_id: tool.id });
    await factory.create("ToolTag", { tool_id: tool.id });
    const response = await request(app)
      .delete(`/tools/${tool.id}`)
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(204);
    const toolDb = await app.models.Tool.findByPk(tool.id);
    const tagsDb = await app.models.ToolTag.findAll({
      where: { tool_id: tool.id },
    });
    expect(!!toolDb).toBe(false);
    expect(tagsDb.length).toBe(0);
  });

  it("should not be able to remove a inexistent tool", async () => {
    const user = await factory.create("User");
    const response = await request(app)
      .delete(`/tools/54878`)
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(404);
  });

  it("should be able to list tools", async () => {
    const user = await factory.create("User");
    const tools = [
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
    ];
    const response = await request(app)
      .get("/tools")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(tools.length);
  });

  it("should be able to list tools filtering by tag", async () => {
    const user = await factory.create("User");
    const tools = [
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
      await factory.create("Tool", { user_id: user.id }),
    ];
    const tags = [
      ["Tag1", "Tag2", "tag3"],
      ["Tag1", "Tag2", "tag3", "TagTeste"],
      ["Tag1", "Tag2", "tag3"],
      ["Tag1", "Tag2", "tag3", "TagTeste"],
      ["Tag1", "Tag2", "tag3"],
    ];
    tools.forEach((tl, i) => {
      tags[i].forEach(async (tg) => {
        await factory.create("ToolTag", { tag: tg, tool_id: tl.id });
      });
    });
    const response = await request(app)
      .get("/tools")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(tools.length);
    const response2 = await request(app)
      .get("/tools?tag=tagteste")
      .set("Authorization", user.generateToken());
    expect(response2.status).toBe(200);
    expect(response2.body.length).toBe(2);
  });

  it("should be able to get details of a especific tool", async () => {
    const user = await factory.create("User");
    const tool = await factory.create("Tool", { user_id: user.id });
    const tags = [
      await factory.create("ToolTag", { tool_id: tool.id }),
      await factory.create("ToolTag", { tool_id: tool.id }),
    ];
    const response = await request(app)
      .get(`/tools/${tool.id}`)
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("id", tool.id);
    expect(response.body.tags.length).toBe(tags.length);
  });

  it("should be able to get details of a inexisting tool", async () => {
    const user = await factory.create("User");
    const response = await request(app)
      .get("/tools/784")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(404);
  });
});
