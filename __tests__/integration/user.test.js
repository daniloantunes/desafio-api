const request = require("supertest");
const app = require("../../src/app");
const { factory, faker } = require("../factories");
const truncate = require("../utils/truncate");

describe("User", () => {
  beforeEach(async () => {
    await truncate();
  });

  it("should view the logged user", async () => {
    const user = await factory.create("User");
    const response = await request(app)
      .get("/user")
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("id");
    expect(response.body).toHaveProperty("email", user.email);
    expect(response.body).toHaveProperty("name", user.name);
  });

  it("should be possible create a user", async () => {
    const name = faker.name.findName();
    const email = faker.internet.email();
    const response = await request(app).post("/user").send({
      name,
      email,
      password: faker.internet.password(),
    });
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("id");
    const user = await app.models.User.findByPk(response.body.id);
    expect(!!user).toBe(true);
    expect(user.id).toBe(response.body.id);
    expect(user.email).toBe(email);
    expect(user.name).toBe(name);
  });

  it("should be possible update the logged user", async () => {
    const user = await factory.create("User");
    const name = faker.name.findName();
    const email = faker.internet.email();
    const response = await request(app)
      .put("/user")
      .send({ name, email })
      .set("Authorization", user.generateToken());
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("id");
    const userUpdated = await app.models.User.findByPk(user.id);
    expect(!!userUpdated).toBe(true);
    expect(userUpdated.id).toBe(user.id);
    expect(userUpdated.email).toBe(email);
    expect(userUpdated.name).toBe(name);
  });
});
