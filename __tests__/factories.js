const app = require('./../src/app');
const { factory } = require('factory-girl');
const faker = require('faker');

factory.define('User', app.models.User, {
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.internet.password()
});

factory.define('Tool', app.models.Tool, {
    user_id: 0,
    title: faker.lorem.word(),
    link: faker.image.imageUrl(),
    description: faker.lorem.text()
});

factory.define('ToolTag', app.models.ToolTag, {
    tool_id: 0,
    tag: faker.lorem.slug()
});

module.exports = { factory, faker };