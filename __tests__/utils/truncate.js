const app = require("../../src/app");

module.exports = () =>
  Promise.all(
    Object.keys(app.models).map((key) => {
      if (key !== "sequelize" && key !== "Sequelize") {
        return app.models[key].destroy({ truncate: true, force: true });
      }
    })
  );
