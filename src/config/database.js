require("dotenv").config();

let env;
switch ((process.env.NODE_ENV || "").trim()) {
  case "prod":
    env = "prod";
    break;
  case "test":
    env = "test";
    break;
  default:
    env = "dev";
}

const database = {
  dev: {
    username: "desafioapidev",
    password: "desafioapidev2020",
    database: "desafioapidev",
    host: "mysql741.umbler.com",
    port: "41890",
    dialect: "mysql",
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true,
    },
  },
  test: {
    username: "desafioapitest",
    password: "desafioapitest2020",
    database: "desafioapitest",
    host: "mysql741.umbler.com",
    port: "41890",
    dialect: "sqlite",
    storage: "./__tests__/database.sqlite",
    logging: false,
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true,
    },
  },
  prod: {
    username: "desafioapiprod",
    password: "desafioapiprod2020",
    database: "desafioapiprod",
    host: "mysql741.umbler.com",
    dialect: "mysql",
    define: {
      timestamps: true,
      underscored: true,
      underscoredAll: true,
    },
  },
};

module.exports = database[env];
