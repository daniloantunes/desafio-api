const os = require("os");

/* importar o módulo do framework express */
const express = require("express");

/* importar o módulo do consign */
const consign = require("consign");

/* importar o módulo do express-validator */
// var expressValidator = require('express-validator');

/* importar o módulo do body-parser */
const bodyParser = require("body-parser");

class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
  }

  middlewares() {
    /* configurar o middleware body-parser */
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(bodyParser.json({ extended: true }));
    this.express.use(bodyParser.text());

    /* configurar o middleware express-validator */
    // app.use(expressValidator());

    this.express.use((req, res, next) => {
      res.header(
        "Access-Control-Allow-Methods",
        "OPTIONS, GET, POST, PUT, DELETE"
      );
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization, Tipo-Usuario, Id-Usuario"
      );
      next();
    });

    // Autoload das rotas, dos controllers e dos models
    const pathConsign = os.type() === "Windows_NT" ? "\\src\\app" : "/src/app";
    consign({ cwd: process.cwd() + pathConsign, verbose: false })
      .include("routes")
      .then("controllers")
      .then("models.js")
      .into(this.express);

    // Criando caminho estático para o arquivo de cobertura dos testes
    this.express.use(
      "/tests",
      express.static(`${__dirname}./../__tests__/coverage/lcov-report`)
    );
  }
}

/* exportar o objeto app */
module.exports = new AppController().express;
