const utils = require("../functions/utils");

getTools = (app, req, tag = null) =>
  app.models.Tool.findAll({
    where: { user_id: req.logged.id },
    include: [{ model: app.models.ToolTag, as: "tags", attributes: ["tag"] }],
  }).then((tools) => {
    tools = tools.map((t) => ({
      id: t.id,
      user_id: t.user_id,
      title: t.title,
      link: t.link,
      description: t.description,
      tags: t.tags.map((t) => t.tag),
    }));
    if (tag) {
      tools = tools.filter((t) =>
        t.tags.toString().toLowerCase().includes(tag.toLowerCase())
      );
    }
    return tools;
  });

module.exports.getTools = (app, req, res) => {
  const { tag } = req.query;
  getTools(app, req, tag).then((tools) => {
    res.json(tools);
  });
};

viewTool = (app, req, id = null) =>
  app.models.Tool.findOne({
    where: { id: id || req.params.id, user_id: req.logged.id },
    include: [{ model: app.models.ToolTag, as: "tags", attributes: ["tag"] }],
  }).then((tool) => {
    if (tool) {
      tool = {
        id: tool.id,
        user_id: tool.user_id,
        title: tool.title,
        link: tool.link,
        description: tool.description,
        tags: tool.tags.map((t) => t.tag),
        createdAt: tool.createdAt,
        updatedAt: tool.updatedAt,
      };
    }
    return tool;
  });

module.exports.viewTool = (app, req, res) => {
  viewTool(app, req).then((tool) => {
    if (!tool) {
      return utils.sendError(res, 404, "Tool not found");
    }
    res.json(tool);
  });
};

module.exports.createTool = async (app, req, res) => {
  const user_id = req.logged.id;
  let { title, link, description, tags } = req.body;
  if (title == undefined || link == undefined || description == undefined) {
    return utils.sendError(res, 400, "Incomplete data");
  }
  const tool = await app.models.Tool.create({
    user_id,
    title,
    link,
    description,
  });
  if (tags != undefined && tags.length) {
    tags = req.body.tags.map((tag) => ({ tool_id: tool.id, tag }));
    const t = await app.models.ToolTag.bulkCreate(tags);
  }
  viewTool(app, req, tool.id).then((tool) => {
    res.status(201).json(tool);
  });
};

module.exports.updateTool = async (app, req, res) => {
  const user_id = req.logged.id;
  let { title, link, description, tags } = req.body;
  const tool = await app.models.Tool.findByPk(req.params.id);
  if (!tool) {
    return utils.sendError(res, 404, "Tool not found");
  }
  await tool.update({ user_id, title, link, description });
  if (tags != undefined && Array.isArray(tags)) {
    tags = tags.map((tag) => ({ tool_id: tool.id, tag }));
    await app.models.ToolTag.destroy({ where: { tool_id: tool.id } });
    await app.models.ToolTag.bulkCreate(tags);
  }
  viewTool(app, req, tool.id).then((tool) => {
    res.status(201).json(tool);
  });
};

module.exports.removeTool = (app, req, res) => {
  app.models.Tool.findOne({
    where: { user_id: req.logged.id, id: req.params.id },
  }).then((tool) => {
    if (!tool) {
      return utils.sendError(res, 404, "Tool not found");
    }
    tool
      .destroy()
      .then((t) => {
        res.status(204).send();
      })
      .catch((e) => utils.sendError(res, 500, e.getMessage()));
  });
};
