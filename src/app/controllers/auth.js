const jwt = require("jsonwebtoken");
const utils = require("../functions/utils");
require("dotenv-safe").config();

newToken = async (user_id) => {
  const { secret } = process.env;
  return jwt.sign(
    {
      id: user_id,
    },
    secret,
    {
      expiresIn: 60 * 30,
    }
  );
};

sendUserLogged = (res, token, user) => {
  user = user.dataValues;
  user.password_hash = undefined;
  res.json({ token, user });
};

module.exports.loginPassword = (app, req, res) => {
  const { email, password } = req.body;
  app.models.User.findOne({ where: { email } }).then((user) => {
    if (user) {
      if (user.checkPassword(password)) {
        newToken(user.id).then((newToken) => {
          sendUserLogged(res, newToken, user);
        });
      } else {
        utils.sendError(res, 401, "Wrong password");
      }
    } else {
      utils.sendError(res, 401, "User not found");
    }
  });
};

module.exports.loginJwt = (app, req, res) => {
  const auth = req.header("Authorization");
  jwt.verify(auth, process.env.secret, (err, decoded) => {
    if (err) {
      return utils.sendError(res, 401, "Falha na autenticação");
    }
    newToken(decoded.id).then((newToken) => {
      app.models.User.findByPk(decoded.id).then((user) => {
        sendUserLogged(res, newToken, user);
      });
    });
  });
};
