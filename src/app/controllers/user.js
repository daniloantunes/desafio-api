const utils = require("../functions/utils");

module.exports.viewUser = (app, req, res) => {
  app.models.User.findByPk(req.logged.id, {
    attributes: ["id", "name", "email", "createdAt", "updatedAt"],
  }).then((user) => {
    res.json(user);
  });
};

module.exports.createUser = (app, req, res) => {
  const { name, email, password } = req.body;
  if (name === undefined || email === undefined || password === undefined) {
    return utils.sendError(res, 400, "Incomplete data");
  }
  app.models.User.create({ name, email, password }).then((user) => {
    user.password = undefined;
    user.password_hash = undefined;
    res.status(201).json(user);
  });
};

module.exports.updateUser = (app, req, res) => {
  const { name, email } = req.body;
  app.models.User.findByPk(req.logged.id, {
    attributes: ["id", "name", "email", "createdAt", "updatedAt"],
  }).then((user) => {
    user.update({ name, email }).then(user => {
      res.status(201).json(user);
    });
  });
};
