module.exports = function (app) {
  app.post("/auth/login", (req, res) => {
    app.controllers.auth.loginPassword(app, req, res);
  });

  app.put("/auth/login", (req, res) => {
    app.controllers.auth.loginJwt(app, req, res);
  });
};
