const path = require("path");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.redirect("/docs");
  });

  app.get("/docs", (req, res) => {
    res.sendFile(path.join(__dirname, "../../../docs/index.html"));
  });
};
