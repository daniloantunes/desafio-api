const validaJwt = require("../middleware/validaJwt");

module.exports = function (app) {
  app.get("/user", validaJwt, (req, res) => {
    app.controllers.user.viewUser(app, req, res);
  });

  app.post("/user", (req, res) => {
    app.controllers.user.createUser(app, req, res);
  });

  app.put("/user", validaJwt, (req, res) => {
    app.controllers.user.updateUser(app, req, res);
  });
};
