const validaJwt = require("../middleware/validaJwt");

module.exports = function (app) {
  app.get("/tools", validaJwt, (req, res) => {
    app.controllers.tools.getTools(app, req, res);
  });

  app.get("/tools/:id", validaJwt, (req, res) => {
    app.controllers.tools.viewTool(app, req, res);
  });

  app.post("/tools", validaJwt, (req, res) => {
    app.controllers.tools.createTool(app, req, res);
  });

  app.put("/tools/:id", validaJwt, (req, res) => {
    app.controllers.tools.updateTool(app, req, res);
  });

  app.delete("/tools/:id", validaJwt, (req, res) => {
    app.controllers.tools.removeTool(app, req, res);
  });
};
