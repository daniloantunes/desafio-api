module.exports.sendError = (res, error_code, error_message) =>
  res
    .status(error_code)
    .json({
      cod: error_code,
      msg: error_message,
    })
    .send();
