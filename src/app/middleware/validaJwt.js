const jwt = require("jsonwebtoken");
const utils = require("../functions/utils");

module.exports = (req, res, next) => {
  const token = req.header("Authorization");
  if (!token) {
    return utils.sendError(res, 401, "Token not provided");
  }
  return jwt.verify(token, process.env.secret, (err, decoded) => {
    if (err) {
      return utils.sendError(res, 401, "Invalid Token");
    }
    req.logged = {
      id: decoded.id,
    };
    return next();
  });
};
