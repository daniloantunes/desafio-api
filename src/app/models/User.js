const passwordHash = require("password-hash");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataType) => {
  const Model = sequelize.define(
    "User",
    {
      id: {
        type: DataType.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataType.STRING(50),
        allowNull: false,
      },
      email: {
        type: DataType.STRING(50),
        allowNull: false,
      },
      password: {
        type: DataType.VIRTUAL,
      },
      password_hash: {
        type: DataType.STRING(56),
      },
    },
    {
      sequelize,
      tableName: "user",
      hooks: {
        beforeSave: async (user) => {
          if (user.password) {
            user.password_hash = passwordHash.generate(user.password);
          }
        },
      },
    }
  );

  Model.prototype.checkPassword = function (password) {
    return passwordHash.verify(password, this.password_hash);
  };

  Model.prototype.generateToken = function () {
    return jwt.sign({ id: this.id }, process.env.secret);
  };

  return Model;
};
