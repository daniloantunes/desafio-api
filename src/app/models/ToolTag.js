module.exports = (sequelize, DataType) => {
  const Model = sequelize.define(
    "ToolTag",
    {
      id: {
        type: DataType.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      tool_id: {
        type: DataType.INTEGER(11),
        allowNull: false,
      },
      tag: {
        type: DataType.STRING(50),
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "tool_tag",
    }
  );

  Model.associate = (models) => {
    Model.belongsTo(models.Tool, {
      as: "tool",
      foreignKey: {
        allowNull: true,
        field: "tool_id",
        name: "tool_id",
      },
    });
  };

  return Model;
};
