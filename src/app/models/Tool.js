module.exports = (sequelize, DataType) => {
  const Model = sequelize.define(
    "Tool",
    {
      id: {
        type: DataType.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      user_id: {
        type: DataType.INTEGER(11),
        allowNull: false,
      },
      title: {
        type: DataType.STRING(50),
        allowNull: false,
      },
      link: {
        type: DataType.STRING(200),
        allowNull: false,
      },
      description: {
        type: DataType.TEXT(2000),
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "tool",
    }
  );

  Model.associate = (models) => {
    Model.hasMany(models.ToolTag, {
      as: "tags",
      hooks: true,
      onDelete: "cascade",
      foreignKey: {
        allowNull: true,
        field: "tool_id",
        name: "tool_id",
      },
    });
  };

  return Model;
};
