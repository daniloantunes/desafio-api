const passwordHash = require("password-hash");

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert("user", [
      {
        name: "Danilo",
        email: "danilo@email.com",
        password_hash: passwordHash.generate("danilo"),
      },
    ]),
  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("users", null, {}),
};
