module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("tool_tag", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      tool_id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },
      tag: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("tool_tag");
  },
};
