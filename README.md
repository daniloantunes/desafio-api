<h1>VUTTR</h1>
<h2>(Very Useful Tools to Remember)</h2>

<h3>
    <a href="http://desafioapi-com-br.umbler.net/docs">Acessar a documentação</a>
</h3>

<h3>
    <a href="http://desafioapi-com-br.umbler.net/tests">Visualizar as estatísticas de cobertura dos testes</a>
</h3>

## Sobre

Esta API foi desenvolvida para o desafio back-end da Bossa Box, e tem como objetivo permitir que um usuário, devidamente cadastrado, faça o gerenciamento das suas ferramentas mais úteis, podendo cadastrar, atualizar, visualizar e remover, além de associar a uma ou mais tags.

---

## Tecnologias utilizadas

- [Node.js] (https://nodejs.org)
- [Express.js] (https://expressjs.com)
- [Sequelize] (https://sequelize.org)
- [MySQL] (https://www.mysql.com)

---

## Como baixar o projeto

```bash
# Clonar o repositório
$ git clone https://gitlab.com/daniloantunes/desafio-api.git

# Entrar no diretório do projeto
cd desafio-api

# Instalar as dependências
$ npm install

# Iniciar o projeto
$ npm start
```

---
Desenvolvido por Danilo Lima Antunes